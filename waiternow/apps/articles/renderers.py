from waiternow.apps.core.renderers import WaiternowJSONRenderer


class ArticleJSONRenderer(WaiternowJSONRenderer):
    object_label = 'article'
    pagination_object_label = 'articles'
    pagination_count_label = 'articlesCount'
