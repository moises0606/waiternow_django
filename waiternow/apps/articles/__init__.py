from django.apps import AppConfig


class ArticlesAppConfig(AppConfig):
    name = 'waiternow.apps.articles'
    label = 'articles'
    verbose_name = 'Articles'

    def ready(self):
        import waiternow.apps.articles.signals

default_app_config = 'waiternow.apps.articles.ArticlesAppConfig'
