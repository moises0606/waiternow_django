from django.db import models

from waiternow.apps.core.models import TimestampedModel


class Article(TimestampedModel):
    slug = models.SlugField(db_index=True, max_length=255, unique=True)
    title = models.CharField(db_index=True, max_length=255)
    body = models.TextField()
    img = models.TextField(blank=True)                                  
    fromwhere = models.TextField(db_index=True, blank=False, default='bar')  #Will be kitchen or bar
    price = models.DecimalField(blank=False, max_digits=7, decimal_places=2, default=0)                                 
    author = models.ForeignKey(
        'profiles.Profile', on_delete=models.CASCADE, related_name='articles'
    )

    def __str__(self):
        return self.title

