from rest_framework import serializers

from waiternow.apps.profiles.serializers import ProfileSerializer

from .models import Article

class ArticleSerializer(serializers.ModelSerializer):
    slug = serializers.SlugField(required=False)
    body = serializers.CharField(required=False)
    title = serializers.CharField(required=False)
    img = serializers.CharField(required=False)
    fromwhere = serializers.CharField(required=False)
    price = serializers.CharField(required=False) 

    # Django REST Framework makes it possible to create a read-only field that
    # gets it's value by calling a function. In this case, the client expects
    # `created_at` to be called `createdAt` and `updated_at` to be `updatedAt`.
    # `serializers.SerializerMethodField` is a good way to avoid having the
    # requirements of the client leak into our API.
    createdAt = serializers.SerializerMethodField(method_name='get_created_at')
    updatedAt = serializers.SerializerMethodField(method_name='get_updated_at')

    class Meta:
        model = Article
        fields = (
            'slug',
            'body',
            'createdAt',
            'title',
            'updatedAt',
            'img',
            'fromwhere',
            'price'
        )

    def create(self, validated_data):
        author = self.context.get('author', None)

        article = Article.objects.create(author=author, **validated_data)

        return article

    def get_created_at(self, instance):
        return instance.created_at.isoformat()


    def get_updated_at(self, instance):
        return instance.updated_at.isoformat()
