from waiternow.apps.core.renderers import WaiternowJSONRenderer


class ProfileJSONRenderer(WaiternowJSONRenderer):
    object_label = 'profile'
    pagination_object_label = 'profiles'
    pagination_count_label = 'profilesCount'
