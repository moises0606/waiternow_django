from rest_framework import serializers

from waiternow.apps.profiles.serializers import ProfileSerializer
from waiternow.apps.articles.serializers import ArticleSerializer

from .models import VirtualTable, Order
from ..articles.models import Article

class VirtualTableSerializer(serializers.ModelSerializer):
    author = ProfileSerializer(read_only=True)
    active = serializers.BooleanField(required=False)

    # Django REST Framework makes it possible to create a read-only field that
    # gets it's value by calling a function. In this case, the client expects
    # `created_at` to be called `createdAt` and `updated_at` to be `updatedAt`.
    # `serializers.SerializerMethodField` is a good way to avoid having the
    # requirements of the client leak into our API.
    createdAt = serializers.SerializerMethodField(method_name='get_created_at')
    updatedAt = serializers.SerializerMethodField(method_name='get_updated_at')

    class Meta:
        model = VirtualTable
        fields = (
            'id_incremental',
            'active',
            'createdAt',
            'author',
            'updatedAt'
        )

    def create(self, validated_data):
        author = self.context.get('author', None)

        vtable = VirtualTable.objects.create(author=author, **validated_data)

        return vtable

    def get_created_at(self, instance):
        return instance.created_at.isoformat()


    def get_updated_at(self, instance):
        return instance.updated_at.isoformat()

class OrderSerializer(serializers.ModelSerializer):
    author = ProfileSerializer(read_only=True)
    article = ArticleSerializer(read_only=True)
    vtable = VirtualTableSerializer(read_only=True)
    served = serializers.BooleanField(required=False)

    createdAt = serializers.SerializerMethodField(method_name='get_created_at')
    updatedAt = serializers.SerializerMethodField(method_name='get_updated_at')

    class Meta:
        model = Order
        fields = (
            'id_incremental',
            'author',
            'article',
            'vtable',
            'served',
            'createdAt',
            'updatedAt',
        )
    def create(self, validated_data):
        author = self.context['author']
        article = Article.objects.get(
                            slug=self.context['article']
                        )
        vtable = VirtualTable.objects.get(
                            id_incremental=self.context['vtable']
                        )
                        
        return Order.objects.create(
            author=author, vtable=vtable, article=article, **validated_data
        )

    def get_created_at(self, instance):
        return instance.created_at.isoformat()

    def get_updated_at(self, instance):
        return instance.updated_at.isoformat()