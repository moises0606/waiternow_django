from django.apps import AppConfig


class VirtualTableAppConfig(AppConfig):
    name = 'waiternow.apps.virtualtable'
    label = 'virtualtable'
    verbose_name = 'VirtualTable'

default_app_config = 'waiternow.apps.virtualtable.VirtualTableAppConfig'
