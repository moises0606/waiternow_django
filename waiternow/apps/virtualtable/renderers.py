from waiternow.apps.core.renderers import WaiternowJSONRenderer


class VirtualTableJSONRenderer(WaiternowJSONRenderer):
    object_label = 'virtualtable'
    pagination_object_label = 'virtualtables'
    pagination_count_label = 'virtualtablesCount'

class OrderJSONRenderer(WaiternowJSONRenderer):
    object_label = 'order'
    pagination_object_label = 'orders'
    pagination_count_label = 'ordersCount'