from django.conf.urls import include, url

from rest_framework.routers import DefaultRouter

from .views import (
    VirtualTableViewSet, VirtualTableFeedAPIView, OrderAPIViewSet, OrderDestroyAPIViewSet, OrderRetrieveUpdateAPIView
)

router = DefaultRouter(trailing_slash=False)
router.register(r'virtualtables', VirtualTableViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),

    url(r'^virtualtables/vt/feed/?$', VirtualTableFeedAPIView.as_view()),
    url(r'^virtualtables/order/(?P<order_id>[-\d]+)?$', OrderRetrieveUpdateAPIView.as_view()),
    url(r'^virtualtables/orders/?$', OrderAPIViewSet.as_view()),
    url(r'^virtualtables/orders/delete/(?P<order_id>[-\d]+)/?$', OrderDestroyAPIViewSet.as_view()),

    # url(r'^/virtualtables/orders/(?P<vtable_id>[-\w]+)/(?P<article_id>[-\w]+)/?$', OrderAPIView.as_view()),
    # url(r'^/orders/?$', OrderAPIView.as_view()),



]
