from rest_framework import generics, mixins, status, viewsets
from rest_framework.exceptions import NotFound
from rest_framework.permissions import (
    AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
)
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import VirtualTable, Order
from ..articles.models import Article
from .renderers import VirtualTableJSONRenderer, OrderJSONRenderer
from .serializers import VirtualTableSerializer, OrderSerializer


class VirtualTableViewSet(mixins.CreateModelMixin, 
                     mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):

    #lookup_field = 'slug'
    queryset = VirtualTable.objects.select_related('author', 'author__user')
    permission_classes = (IsAuthenticatedOrReadOnly,)
    renderer_classes = (VirtualTableJSONRenderer,)
    serializer_class = VirtualTableSerializer
    
    def get_queryset(self):
        queryset = self.queryset

        author = self.request.query_params.get('author', None)

        if author is not None:
            queryset = queryset.filter(author__user__username=author)


        return queryset

    def create(self, request):
        serializer_context = {
            'author': request.user.profile,
            'request': request
        }
        serializer_data = request.data.get('virtualtable', {})

        serializer = self.serializer_class(
        data=serializer_data, context=serializer_context
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )

        return self.get_paginated_response(serializer.data)

    def update(self, request, pk):
        serializer_context = {'request': request}
        
        try:
            serializer_instance = self.queryset.get(id_incremental=pk)
        except VirtualTable.DoesNotExist:
            raise NotFound('The virtualtable with this id does not exist.')

        serializer_data = request.data.get('virtualtable', {})
        serializer = self.serializer_class(
            serializer_instance, 
            context=serializer_context,
            data=serializer_data, 
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk):
        try:
            virtualtable = VirtualTable.objects.get(id_incremental=pk)
        except VirtualTable.DoesNotExist:
            raise NotFound('The virtual table with this id does not exist.')

        virtualtable.delete()
        return Response(None, status=status.HTTP_204_NO_CONTENT)
    
class VirtualTableFeedAPIView(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = VirtualTable.objects.all()
    renderer_classes = (VirtualTableJSONRenderer,)
    serializer_class = VirtualTableSerializer

    def get_queryset(self):
        return VirtualTable.objects.filter(
            author__in=[self.request.user.profile],
            active=True
        )
        
    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )
        return self.get_paginated_response(serializer.data)

class OrderAPIViewSet(generics.ListCreateAPIView,): 
    permission_classes = (IsAuthenticated,)
    queryset = Order.objects.all()
    renderer_classes = (OrderJSONRenderer,)
    serializer_class = OrderSerializer

    def get_queryset(self):
        return Order.objects.filter(
            author__in=[self.request.user.profile]
        )
    
    def create(self, request):
        serializer_context = {
            'author': request.user.profile,
            'article': request.data.get('article', {}),
            'vtable': request.data.get('vtable', {}),
            'request': request
        }

        serializer_data = request.data.get('order', {})

        serializer = self.serializer_class(
        data=serializer_data, context=serializer_context
        )

        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def list(self, request):
        serializer_context = {'request': request}
        page = self.paginate_queryset(self.get_queryset())

        serializer = self.serializer_class(
            page,
            context=serializer_context,
            many=True
        )
        return self.get_paginated_response(serializer.data)

class OrderRetrieveUpdateAPIView(generics.RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Order.objects.all()
    renderer_classes = (OrderJSONRenderer,)
    serializer_class = OrderSerializer
    
    def update(self, request, order_id):
        serializer_context = {'request': request}

        try:
            serializer_instance = self.queryset.get(id_incremental=order_id)
        except Order.DoesNotExist:
            raise NotFound('The order with this id does not exist.')

        serializer_data = request.data.get('order', {})
        serializer = self.serializer_class(
            serializer_instance, 
            context=serializer_context,
            data=serializer_data, 
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

class OrderDestroyAPIViewSet(generics.ListCreateAPIView,
                        generics.DestroyAPIView):
                
    permission_classes = (IsAuthenticated,)
    queryset = Order.objects.all()
    renderer_classes = (OrderJSONRenderer,)
    serializer_class = OrderSerializer

    def destroy(self, request, order_id):
        try:
            order = Order.objects.get(id_incremental=order_id)
        except Order.DoesNotExist:
            raise NotFound('The order table with this id does not exist.')

        order.delete()
        return Response(None, status=status.HTTP_204_NO_CONTENT)