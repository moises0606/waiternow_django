from django.db import models

from waiternow.apps.core.models import TimestampedModel


class VirtualTable(TimestampedModel):
    id_incremental = models.AutoField(primary_key=True)
    active = models.BooleanField(default=True) #0 Table open, 1 Table Closed
    author = models.ForeignKey(
        'profiles.Profile', on_delete=models.CASCADE, related_name='VirtualTable'
    )

class Order(TimestampedModel):
    id_incremental = models.AutoField(primary_key=True)
    served = models.BooleanField(default=False) #0 Served, 1 Not Served
    vtable = models.ForeignKey('virtualtable.VirtualTable', db_index=True, related_name='Order')
    article = models.ForeignKey('articles.Article', db_index=True, related_name='Order')
    author = models.ForeignKey('profiles.Profile', db_index=True, related_name='Order')

